package zjhzcc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class QuestionsServlet
 */
@WebServlet("/questions.do")
public class QuestionsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out=response.getWriter();
		try {
		int score=0;
		int a1 =Integer.parseInt(request.getParameter("quest1"));//判断第一题
		if(a1==1) {
			score=score+10;
		}else {
			score=score+0;
		}
		int a2 =Integer.parseInt(request.getParameter("quest2"));//判断第二题
		if(a2==3) {
			score=score+10;
		}else {
			score=score+0;
		}

		String a3[]=request.getParameterValues("quest3");//判断第三题
		String a3str=Arrays.toString(a3);
		if(a3!=null){
			//多选，正确答案AC
			if(a3.length==3 || a3.length==0 ||a3str.indexOf("2")!=-1){ //多选或不选或错选
			score=score+0;
			}
			else if(a3str.indexOf("3")==4){ //全对
			score=score+10;
			}
			else{//只答对一题
			score=score+5;
			}
			}
			else{
			score=score+0;
			}
		String a4[]=request.getParameterValues("quest4");//判断第四题
		String a4str=Arrays.toString(a4);
		if(a4str!=null && a4str.equalsIgnoreCase("[HttpServlet]")) {
			score=score+10;
		}else {
			score=score+0;
		}
	
		
		out.print("你的成绩是"+score);	  
		}catch (Exception e) {
			// TODO: handle exception
			out.print("你似乎并没有做完所有题目哦，请选择你的答案后再提交！");
		}
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
